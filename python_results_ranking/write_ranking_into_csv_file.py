import csv

def write_ranking_into_csv_file(tuples_already_processed, csv_file_path):
    result_file = open(csv_file_path, 'a', encoding="UTF-8", newline='')
    csv_writer = csv.writer(result_file)
    csv_writer.writerow(["File 1","File 2","Total scored sentences","List of sentences score equal 1","List of sentences score 0.7 to 1","List of sentences score 0.5 to 0.7"])
    
    for tuple_item in tuples_already_processed.keys():
        total_list_of_scored_sentences = len(tuples_already_processed[tuple_item]["list_of_score_equal_1"]) + len(tuples_already_processed[tuple_item]["list_of_score_07_to_1"]) + len(tuples_already_processed[tuple_item]["list_of_score_05_to_07"])

        csv_writer.writerow([tuples_already_processed[tuple_item]["pdffile1"]]+[tuples_already_processed[tuple_item]["pdffile2"]]+[total_list_of_scored_sentences]+[len(tuples_already_processed[tuple_item]["list_of_score_equal_1"])]+[len(tuples_already_processed[tuple_item]["list_of_score_07_to_1"])]+[len(tuples_already_processed[tuple_item]["list_of_score_05_to_07"])])

    result_file.close()
