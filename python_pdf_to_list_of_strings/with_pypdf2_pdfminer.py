# importing required modules
import PyPDF2
from .with_pypdf2_pdfminer_data_func import convert_pdf_to_string
from .sanitize_text import remove_special_chars_from_string, remove_only_special_chars_from_string
import os 
import re 
import copy 
import csv

###############################################################################
# Raw text
###############################################################################

def extract_text_from_pdf_file(pdf_file_name):
    script_dir = os.path.dirname(__file__)
    rel_path = f"../files_to_compare/pdf/{pdf_file_name}"
    abs_file_path = os.path.join(script_dir, rel_path)

    text = convert_pdf_to_string(abs_file_path)     
    return text

def sanitize_text(text):
    sanitized_text = remove_special_chars_from_string(text)
    return sanitized_text

def write_text_into_file(text,file_name,result_folder_name="tmp"):
    file_path = f"./results/{result_folder_name}/{file_name}.txt"
    result_file = open(file_path, 'w', newline='', encoding="UTF-8")
    result_file.write(remove_only_special_chars_from_string(text))
    result_file.close()
    return file_path

def write_list_strings_into_file(list_of_strings,file_name,result_folder_name="raw",sentence_minimal_length=8):
    file_path = f"./results/{result_folder_name}/{file_name}.txt"
    result_file = open(file_path, 'w', newline='', encoding="UTF-8")
    for item in list_of_strings:
        if len(item) >= sentence_minimal_length:
            result_file.write(f"{item}\n")
    result_file.close()
    return file_path

def extract_sanatized_text_from_pdf_file(pdf_file_name):
    text = extract_text_from_pdf_file(pdf_file_name)
    sanitized_text = sanitize_text(text)

    return sanitized_text

###############################################################################
# Sentences
###############################################################################

def separate_list_of_sentences_with_delimiter(list_of_sentences,sentence_minimal_length=7):
    final_sentences=[]
    
    # print(f"text_as_sentences_list : {text_as_sentences_list}")
    # print(f"len(text_as_sentences_list) : {len(text_as_sentences_list)}")
    for sentence in list_of_sentences:
        splitted_sentences_list = sentence.split(". ")
        for splitted_sentence in splitted_sentences_list:
            if len(splitted_sentence) > sentence_minimal_length :
                tmp_sentence = (splitted_sentence + '.')[:-1] #need to create a new string ref
                # if re.match('([0-9])|(\s)',sentence[0]):
                #     while ((len(tmp_sentence) > 0) and re.match('([0-9])|(\s)',tmp_sentence[0]) != None) :
                #         tmp_sentence = tmp_sentence[:0] + tmp_sentence[(1):] #need to cut the new string
                # # print(f"Sentence after while : {tmp_sentence}")
                final_sentences.append(splitted_sentence)
    return final_sentences

def get_list_of_sentences_from_file(file_path,sentence_minimal_length=7):
    
    list_of_sentences = []
    f = open(file_path, 'r', newline='', encoding="UTF-8")
    for line in f:
        list_of_sentences.append(line)
    f.close()

    # print(list_of_sentences)        
    current_sentence = ""
    list_of_refined_sentences = []
    
    # print(f"len(list_of_sentences) : {len(list_of_sentences)}")
    for i in range(len(list_of_sentences)): 
        # print(f"list_of_sentences[{i}] : {list_of_sentences[i]}") 
        # print(f"current_sentence (beggining): {current_sentence}") 

        # -> (re)initializing the loop variables
        sentence_added = False

        # -> if new paragraph, initiate the current sentence
        if current_sentence == "" and sentence_added == False:
            # print(f"(re)initiate the current_sentence ({i}) if not already added")
            current_sentence = list_of_sentences[i]

        # -> Continue to the next one if empty line
        if list_of_sentences[i] == "\n":
            sanitized_sentence = sanitize_text(current_sentence)
            if len(sanitized_sentence) >= sentence_minimal_length:
                list_of_refined_sentences.append(sanitize_text(current_sentence))
                sentence_added = True
                current_sentence = ""
        elif current_sentence != list_of_sentences[i]:
            current_sentence = (current_sentence + ' ') + list_of_sentences[i]
        elif sentence_added==False and (i == len(list_of_sentences)-1) :
            sanitized_sentence = sanitize_text(current_sentence)
            if len(sanitized_sentence) >= sentence_minimal_length:
                list_of_refined_sentences.append(sanitize_text(current_sentence))

        # print(f"current_sentence (end): {current_sentence}") 
        # print(f"list_of_refined_sentences: {list_of_refined_sentences}")

    return list_of_refined_sentences


def extract_sanatized_sentences_from_pdf_file(pdf_file_name, sentence_minimal_length=7):
    
    sanitized_text = extract_text_from_pdf_file(pdf_file_name)

    pdf_file_name_without_extension = re.sub(r'.pdf', '', pdf_file_name)
    
    sanitized_text_file_path = write_text_into_file(
        file_name=pdf_file_name_without_extension,
        text=sanitized_text,
    )
    
    sanitized_list_of_sentences = get_list_of_sentences_from_file(
        file_path=sanitized_text_file_path,
        sentence_minimal_length=sentence_minimal_length
    )

    separated_list_of_sentences = separate_list_of_sentences_with_delimiter(
        list_of_sentences = sanitized_list_of_sentences,
        sentence_minimal_length = sentence_minimal_length
    )
    
    write_list_strings_into_file(
        file_name=pdf_file_name_without_extension,
        list_of_strings=separated_list_of_sentences,
        sentence_minimal_length=sentence_minimal_length
    )
    # print(f"sanitized_list_of_sentences : {sanitized_list_of_sentences}")
    return separated_list_of_sentences