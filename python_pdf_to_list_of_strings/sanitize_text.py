import re

list_of_special_chars = [
    '\uf0a7',
    '\uf0b7',
    '\uff10',
    '\uff11',
    '\uff12',
    '\uff13',
    '\uff14',
    '\uff15',
    '\uff16',
    '\uff17',
    '\uff18',
    '\uff19',
    '\u2010',
    '\u2103',
    '\u2160',
    '\u23ae',
    '\u25fc',
    '\u2028',
    '\u25fb',
    '♀'
]

def remove_special_chars_from_string(text):
    for special_char in list_of_special_chars:
        text = re.sub(r"{}".format(special_char), '', text)
    text = re.sub(r'\n ', '', text)
    text = re.sub(r'\n', '', text)
    text = re.sub(r'•', '.', text)
    text = re.sub(r'“', '"', text)
    text = re.sub(r'”', '"', text)
    text = re.sub(r'\x0c', ' ', text)
    text = re.sub(r'１', '1', text)
    text = re.sub(r'４', '4', text)
    text = re.sub(r'courtesy of Climate Bonds Initiative June 2020', ' ', text)
    text = re.sub(r'(\(Unofficial translation courtesy of the Climate Bonds Initiative\)  Notice on Issuing the Green Bond Endorsed Projects Catalogue \(2021 Edition\) \(No)', ' ', text)
    return text

def remove_only_special_chars_from_string(text):
    for special_char in list_of_special_chars:
        text = re.sub(r"{}".format(special_char), '', text)
    text = re.sub(r'•', '.', text)
    text = re.sub(r'“', '"', text)
    text = re.sub(r'”', '"', text)
    text = re.sub(r'\x0c', ' ', text)
    text = re.sub(r'１', '1', text)
    text = re.sub(r'４', '4', text)
    return text
