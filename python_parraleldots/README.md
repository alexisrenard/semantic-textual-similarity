## Usage of the parraleldots API 

## Overwiew 

Run API queries to check the similarity between two texts. 

## Results 

```shell 
curl -X POST \
-F "text_1=<text_1>" -F "text_2=<text_2>" -F 'api_key=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' https://apis.paralleldots.com/v4/similarity 
```

## Useful links 
* https://apis.paralleldots.com/text_docs/index.html#similarity