from sentence_transformers import SentenceTransformer, util
from datetime import datetime
import paralleldots
import getpass
import json
import csv

from python_pdf_to_list_of_strings.with_pypdf2_pdfminer import extract_sanatized_text_from_pdf_file, extract_sanatized_sentences_from_pdf_file
from python_results_ranking.write_ranking_into_csv_file import write_ranking_into_csv_file

###############################################################################
# ENV VARIABLES 
###############################################################################

RUN_SEMANTIC_SCORE_SIMILARITY = False
RUN_SENTENCE_TRANSFORMER = True
RUN_COMPUTE_THE_RESULTS_FILE_SUMMARIZED = True
ASK_FOR_RESULT_FILE_PREFIX_TO_USER = False
SENTENCE_MINIMAL_LENGTH = 7
DEFAULT_CSV_FILES_FOLDER_PATH = "./files_to_compare/csv"
DEFAULT_PDF_FILES_FOLDER_PATH = "./files_to_compare/pdf"
DEFAULT_COMPARISON_CSV_FILE_NAME = "default_list_to_compare.csv"
DEFAULT_RESULT_FOLDER_PATH = "./results"
DEFAULT_RESULT_FILE_SUFFIX = "semantic_score_similarity"
CURRENT_DATE = f"{datetime.today().strftime('%Y-%m-%d-%H_%M')}-"

###############################################################################
# GET SOME SCRIPT INPUT FROM THE USER
###############################################################################

def get_result_file_prefix():
    result_file_prefix = ""
    result_file_prefix = input('Please input a file prefix if needed (press enter for no prefix) :')
    if result_file_prefix != "":
        result_file_prefix = result_file_prefix + "-"
    return result_file_prefix

if ASK_FOR_RESULT_FILE_PREFIX_TO_USER:
    result_file_prefix = get_result_file_prefix()
else:
    result_file_prefix = ""

def get_comparison_csv_file_name(default_comparison_csv_file_name):
    comparison_csv_file_name = default_comparison_csv_file_name
    input_comparison_csv_file_name = input('Please input the name of the comparison csv file to read (defaults to `default_list_to_compare.csv` if pressing enter) :')
    if input_comparison_csv_file_name != "":
        comparison_csv_file_name = input_comparison_csv_file_name
    return comparison_csv_file_name

comparison_csv_file_name = get_comparison_csv_file_name(DEFAULT_COMPARISON_CSV_FILE_NAME)

# -> Add prefix if not using the default comparison csv file
print(f"result_file_prefix : {result_file_prefix}")
print(f"comparison_csv_file_name : {comparison_csv_file_name}")
if comparison_csv_file_name != DEFAULT_COMPARISON_CSV_FILE_NAME:
    result_file_prefix += f"{comparison_csv_file_name.split('.')[0]}-"

###############################################################################
# GET THE PDFS TUPLES TO COMPARE
###############################################################################

list_of_pdf_to_compare = []
f = open(f"{DEFAULT_CSV_FILES_FOLDER_PATH}/{comparison_csv_file_name}", 'r', encoding="UTF-8", newline='')
csv_reader = csv.reader(f)
for line in csv_reader:
    # print(line)
    list_of_pdf_to_compare.append((f"{line[0]}.pdf",f"{line[1]}.pdf"))
f.close()

print(list_of_pdf_to_compare)

###############################################################################
# OPEN THE RESULT CSV FILE AND SET UP WRITER
###############################################################################

result_file = open(f"{DEFAULT_RESULT_FOLDER_PATH}/{CURRENT_DATE}{result_file_prefix}{DEFAULT_RESULT_FILE_SUFFIX}.csv", 'a', encoding="UTF-8", newline='')
csv_writer = csv.writer(result_file)

###############################################################################
# RUN THE COMPARISON FOR EVERY LISTED FILES 
###############################################################################

progress_indice = 1
files_already_processed = {}
tuples_already_processed = {}
lenght_of_list = len(list_of_pdf_to_compare)

for (pdffile1,pdffile2) in list_of_pdf_to_compare:
    #this progress_indice is useful to target specific documents or from a specific range
    #defaults to `if progress_indice >= 0`
    if (progress_indice >= 0 ) and (f"{pdffile1}-{pdffile2}" not in tuples_already_processed.keys()):
        print(f"Analysing {pdffile1} with {pdffile2}... ({progress_indice}/{lenght_of_list})")
        
        current_tuple_key = f"{pdffile1}{pdffile2}"

        csv_writer.writerow(["Comparison index",progress_indice])
        csv_writer.writerow(["Name of the first document",pdffile1])
        csv_writer.writerow(["Name of the second document",pdffile2])

        #===================================================
        # SEMANTIC SCORE SIMILARITY
        #===================================================

        if RUN_SEMANTIC_SCORE_SIMILARITY :
            # api_key = getpass.getpass('API Key:')
            # paralleldots.set_api_key(api_key)

            # -> Get the sanitized text from the pdf files
            text1 = extract_sanatized_text_from_pdf_file(pdf_file_name=pdffile1)
            text2 = extract_sanatized_text_from_pdf_file(pdf_file_name=pdffile2)

            # -> Get the similarity scores between PDFs
            # similarity_score = paralleldots.similarity(text1,text2)
            similarity_score = "{\"similarity_score\": 0.9749664664}"
            similarity_score = json.loads(similarity_score)
            print(similarity_score)
            row = ["Global similarity score",similarity_score["similarity_score"]]
            # -> Write into csv the results
            csv_writer.writerow(row)

        #===================================================
        # SENTENCE TRANSFORMER
        #===================================================

        if RUN_SENTENCE_TRANSFORMER :
            model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

            # Check if the files have previously been processed
            if str(pdffile1) in files_already_processed.keys():
                sentences1 = files_already_processed[str(pdffile1)]
            else :
                sentences1 = extract_sanatized_sentences_from_pdf_file(pdf_file_name=pdffile1,sentence_minimal_length=SENTENCE_MINIMAL_LENGTH)
                files_already_processed[str(pdffile1)] = sentences1
            if str(pdffile2) in files_already_processed.keys():
                sentences2 = files_already_processed[str(pdffile2)]
            else :
                sentences2 = extract_sanatized_sentences_from_pdf_file(pdf_file_name=pdffile2,sentence_minimal_length=SENTENCE_MINIMAL_LENGTH)
                files_already_processed[str(pdffile2)] = sentences2   
        
            # Compute embedding for both lists
            embeddings1 = model.encode(sentences1, convert_to_tensor=True)
            embeddings2 = model.encode(sentences2, convert_to_tensor=True)

            # Compute cosine-similarits
            cosine_scores = util.pytorch_cos_sim(embeddings1, embeddings2)

            #----------------------
            # Process to the scoring of the document comparison
            #----------------------

            tuples_already_processed[current_tuple_key] = {}

            list_of_score_equal_1 = []
            list_of_score_07_to_1 = []
            list_of_score_05_to_07 = []

            # Output the pairs with their score
            for i in range(min(len(sentences1),len(sentences2))):
                if cosine_scores[i][i] == 1 :
                    list_of_score_equal_1.append((sentences1[i],sentences2[i],format(cosine_scores[i][i].item(),".3f")))
                if cosine_scores[i][i] >= 0.7 and cosine_scores[i][i] < 1 :
                    list_of_score_07_to_1.append((sentences1[i],sentences2[i],format(cosine_scores[i][i].item(),".3f")))
                if cosine_scores[i][i] >= 0.5 and cosine_scores[i][i] < 0.7 :
                    list_of_score_05_to_07.append((sentences1[i],sentences2[i],format(cosine_scores[i][i].item(),".3f")))

            csv_writer.writerow(["Sentences with a similarity score == 1","Score","Sentence1","Sentence2"])
            for tuple_element in sorted(list_of_score_equal_1, key=lambda tup: tup[2], reverse=True):
                csv_writer.writerow([""]+[tuple_element[2]]+[tuple_element[0]]+[tuple_element[1]])

            csv_writer.writerow(["Sentences with a similarity score >= 0.7 and < 1","Score","Sentence1","Sentence2"])
            for tuple_element in sorted(list_of_score_07_to_1, key=lambda tup: tup[2], reverse=True):
                csv_writer.writerow([""]+[tuple_element[2]]+[tuple_element[0]]+[tuple_element[1]])

            csv_writer.writerow(["Sentences with a similarity score >= 0.5 and < 0.7","Score","Sentence1","Sentence2"])
            for tuple_element in sorted(list_of_score_05_to_07, key=lambda tup: tup[2], reverse=True):
                csv_writer.writerow([""]+[tuple_element[2]]+[tuple_element[0]]+[tuple_element[1]])


            tuples_already_processed[current_tuple_key]["pdffile1"] = pdffile1
            tuples_already_processed[current_tuple_key]["pdffile2"] = pdffile2
            tuples_already_processed[current_tuple_key]["list_of_score_equal_1"] = list_of_score_equal_1
            tuples_already_processed[current_tuple_key]["list_of_score_07_to_1"] = list_of_score_07_to_1
            tuples_already_processed[current_tuple_key]["list_of_score_05_to_07"] = list_of_score_05_to_07

        csv_writer.writerow("")
        csv_writer.writerow("")

        
    progress_indice+=1
# close the file
result_file.close()

###############################################################################
# COMPUTE THE RESULTS FILE SUMMARIZED
###############################################################################

if RUN_COMPUTE_THE_RESULTS_FILE_SUMMARIZED:
    results_file_summarized_path = f"{DEFAULT_RESULT_FOLDER_PATH}/{CURRENT_DATE}{result_file_prefix}{DEFAULT_RESULT_FILE_SUFFIX}_summarized.csv"
    write_ranking_into_csv_file(tuples_already_processed=tuples_already_processed,csv_file_path=results_file_summarized_path)

