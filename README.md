## Overview 

This repo is intended to be used as a base by anyone with needs to parse pdf, analyse semantic textual similarity between them, and explore the results based on an csv output. It uses the [BERT](https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html) algorithm to perform the analysis. The pre-trained model used here is [paraphrase-MiniLM-L6-v2](https://huggingface.co/sentence-transformers/paraphrase-MiniLM-L6-v2) and it is definitely customizable.

This project is intended to be generic, and is open to contribution. 

## Useful links 
* Sentence Transformers in the Hugging Face Hub : ehttps://huggingface.co/blog/sentence-transformers-in-the-hub
* A python library (integrated with Huggingface Model Hub) : https://github.com/UKPLab/sentence-transformers
    * Documentation : https://www.sbert.net/docs/installation.html
    * Use case examples : https://github.com/UKPLab/sentence-transformers/tree/master/examples/applications
* The Best Document Similarity Algorithm in 2020 : https://towardsdatascience.com/the-best-document-similarity-algorithm-in-2020-a-beginners-guide-a01b9ef8cf05
* An API : https://radimrehurek.com/gensim/intro.html#what-is-gensim
* An API : https://dandelion.eu/profile/plans-and-pricing/
* An API : https://komprehend.io/
    * https://apis.paralleldots.com/text_docs/index.html
    * https://github.com/ParallelDots/ParallelDots-Python-API
* A service : https://www.bytesview.com/text-analysis-for-research


## About the author

It has been writing by [Alexis Renard](https://fr.linkedin.com/in/renardalexis), computer science engineer as a side project. 
